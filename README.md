# 仕様書01（RailsによるWebアプリ：テーブル1個のみ）

## 仕様
- スタバの商品リストを作成する。表示項目は[商品名]と[単価]とする。作成したプロジェクトはBitBucketにアップロードし、Herokuにデプロイして動作確認すること。

## 名称
- アプリ名：starbucks
- テーブル名：item
- 商品名(name:string)
- 単価(price:integer)

## 1. プロジェクトの作成
```  
cd hum/rails
rails _5.0.0.1_ new starbucks
cd starbucks
cp ../Gemfile .
bundle update
```
## 2. テーブルの作成
```
rails g scaffold item name:string price:integer
spring stop
rails db:migrate
```
## 3. Rootの設定
```
atom .
config/routes.rb  → root ‘items#index’
rails s
```

- http://localhost:3000/

## 4. 日本語化
```
cp ~/hum/rails/ja.yml config/locales
cp ~/hum/rails/i18n.rb config/initializers
```

- app/views/itemsにある*.html.erbを編集する

## 5. Gitに登録
```
git init
git add -A
git commit -m “scaffold & locales”
```
## 6. Herokuの準備
- Gemfileの末尾に次の3行を追加
```
group :production do
  gem ‘pg’ , ‘0.18.4’
end
```

## 7. BitBucketにアップロード
```
git remote add origin git@bitbucket.org:nariifm/starbucks.git
git push -u origin --all
```

## 8. Herokuにデプロイ(配置)
```
bundle install --without production
git commit -a -m "Update Gemfile.lock for Heroku"
heroku create
git push heroku master
heroku run rails db:migrate
heroku open
```
