Rails.application.routes.draw do
  get 'sales/list'
  resources :sales
  resources :items
  root 'sales#index'
end
