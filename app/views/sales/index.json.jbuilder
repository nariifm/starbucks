json.array!(@sales) do |sale|
  json.extract! sale, :id, :date, :item_id, :price
  json.url sale_url(sale, format: :json)
end
